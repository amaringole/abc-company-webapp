package com.tcs.abccompany.webapp.service;

import com.tcs.abccompany.webapp.model.OrderData;
import com.tcs.abccompany.webapp.repository.OrderDataRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    private OrderDataRepo orderDataRepo;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void getOrders() {

        OrderData orderData1 = new OrderData(1, "product1", 2, 12345.67);
        OrderData orderData2 = new OrderData(1, "product1", 2, 12345.67);
        List<OrderData> orderDataList = new ArrayList<>();
        orderDataList.add(orderData1);
        orderDataList.add(orderData2);
        //When
        Mockito.when(orderDataRepo.findAll()).thenReturn(orderDataList);
        List<OrderData>  response  = orderService.getOrders();
        assertEquals(2, response.size());
    }
}