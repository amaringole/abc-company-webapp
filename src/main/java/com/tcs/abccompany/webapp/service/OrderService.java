package com.tcs.abccompany.webapp.service;

import com.tcs.abccompany.webapp.model.OrderData;
import com.tcs.abccompany.webapp.repository.OrderDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
 
    @Autowired
    private OrderDataRepo orderDataRepo;
 

 
    public List<OrderData> getOrders() {
       return orderDataRepo.findAll();
    }

    public OrderData getOrderDetails(Integer orderId) {
        return orderDataRepo.findById(orderId);
    }
 
}