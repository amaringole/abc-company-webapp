package com.tcs.abccompany.webapp.repository;

import com.tcs.abccompany.webapp.model.OrderData;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OrderDataRepo {

    public Map<Integer, OrderData> data = new HashMap<>();

    @PostConstruct
    public void init() {
        data.put(1001, new OrderData(1001, "Product 1", 2, 1500.00));
        data.put(2001, new OrderData(2001, "Product 2", 2, 1450.45));
        data.put(3001, new OrderData(3001, "Product 3", 2, 1000.00));
    }

    public List<OrderData> findAll() {
        List<OrderData> orderDataList = new ArrayList<>();
        orderDataList.addAll(data.values());
        return orderDataList;
    }
    public OrderData findById(Integer id) {
        return data.get(id);
    }
}
