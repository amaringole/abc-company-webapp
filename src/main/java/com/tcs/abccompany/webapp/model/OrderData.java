package com.tcs.abccompany.webapp.model;

import java.math.BigDecimal;

public class OrderData {
 
    private int id;
    private String productName;
    private int count;
    private double price;

    public OrderData(int id, String productName, int count, double price) {
        this.id = id;
        this.productName = productName;
        this.count = count;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}