package com.tcs.abccompany.webapp.controller;

import com.tcs.abccompany.webapp.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/order")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/viewOrders")
    public String viewBooks(Model model) {
        model.addAttribute("orders", orderService.getOrders());
        return "view-orders";
    }

    @GetMapping("/details/{orderId}")
    public String getDetails(Model model, @PathVariable("orderId") Integer orderId) {
        System.out.println("Received orderId: " + orderId);
        model.addAttribute("order", orderService.getOrderDetails(orderId));
        return "order-details";
    }
}