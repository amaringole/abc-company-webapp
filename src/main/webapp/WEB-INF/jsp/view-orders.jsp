<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>View Books</title>
        <link href="<c:url value="/css/common.css"/>" rel="stylesheet" type="text/css">
        <style>
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }

            td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }

            tr:nth-child(even) {
              background-color: #dddddd;
            }
        </style>
    </head>
    <body>
        <h1> ABC Company Portal </h1>
        <hr>
        <h3> Order Details </h3>
        <table>
            <tr>
                <th>Name</th>
                <th>Count</th>
                <th>Price</th>
            </tr>
          <c:forEach items="${orders}" var="order">
                <tr>
                    <td><a href="/abc-company/order/details/${order.id}">${order.productName}</a></td>
                    <td>${order.count}</td>
                    <td>${order.price}</td>
                </tr>
            </c:forEach>

        </table>
    </body>
</html>