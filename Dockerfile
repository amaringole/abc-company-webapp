FROM openjdk:8-jdk-alpine
MAINTAINER abccompany.com
COPY target/abc-company-webapp-1.0.war abc-company-webapp-1.0.war
ENTRYPOINT ["java","-jar","/abc-company-webapp-1.0.war"]